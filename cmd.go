// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package npm

import (
	"fmt"
	"os"
	"path"
	"path/filepath"

	"go.uber.org/zap"
)

// DownloadDependencies for project.json
func (r *Registry) DownloadDependencies(baseDir, dstDir string, updateLock bool) error {
	packageFilePath := filepath.Join(baseDir, "package.json")
	packageLockFilePath := filepath.Join(baseDir, "package-lock.json")

	zap.S().Infof("Load %s", packageFilePath)
	packageFile, err := LoadPackageFile(packageFilePath)
	if err != nil {
		return err
	}

	zap.L().Info("Load package information from registry")
	packages := make(map[string]*Package)
	for key := range packageFile.Dependencies {
		packages[key], err = r.GetPackage(key)
		if err != nil {
			return fmt.Errorf("failed to get package %s: %v", key, err)
		}
	}

	// lock data does not exist -> create new
	var lockFile *PackageLockFile
	if _, err := os.Stat(packageLockFilePath); os.IsNotExist(err) {
		zap.S().Infof("Create new lock file: %s", packageLockFilePath)
		lockFile = NewPackageLockFile(packageFile.Name, packageFile.Version)

		// new lock file requires update
		updateLock = true
	} else {
		zap.S().Infof("Load lock file: %s", packageLockFilePath)
		lockFile, err = LoadPackageLockFile(packageLockFilePath)
		if err != nil {
			return err
		}
	}

	// update lock file if not exist or requested
	if updateLock {
		zap.S().Info("Update lock file:")

		for key, pack := range packages {
			entry, err := pack.GetLatestVersion(packageFile.Dependencies[key])
			if err != nil {
				return err
			}

			version, ok := lockFile.GetDependencyVersion(key)
			if !ok || version != entry.Version.String() {
				if version != "" {
					zap.S().Infof("  update %s: %s => %s", key, version, entry.Version.String())
				}
				lockFile.SetDependencyVersion(key, entry.Version.String())
			}
		}

		err = SavePackageLockFile(packageLockFilePath, lockFile)
		if err != nil {
			return err
		}
	}

	zap.L().Info("Download packages:")
	for key, pack := range packages {
		version, ok := lockFile.GetDependencyVersion(key)
		if !ok {
			zap.S().Infof("  %s: version missing -> update lock file", key)

			// get latest version
			entry, err := pack.GetLatestVersion(packageFile.Dependencies[key])
			if err != nil {
				return err
			}

			// update version
			version = entry.Version.String()
			lockFile.SetDependencyVersion(key, version)

			// save lock file
			err = SavePackageLockFile(packageLockFilePath, lockFile)
			if err != nil {
				return err
			}
		}
		entry, ok := pack.Versions[version]
		if !ok {
			return fmt.Errorf("version %s of %s does not exist", version, key)
		}

		if entry.Exist(path.Join(dstDir, key)) {
			zap.S().Infof("  %s: up to date", key)
			continue
		}
		zap.S().Infof("  %s: download %s", key, version)

		err = entry.Download(path.Join(dstDir, key))
		if err != nil {
			return err
		}
	}
	zap.S().Info("Download complete")
	return nil
}
