// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package npm

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"sort"
	"strings"

	"github.com/Masterminds/semver"
	"github.com/codeclysm/extract"
	"go.uber.org/zap"
)

// Registry provides interface to NPM registry
type Registry struct {
	HTTPClient *http.Client
	URL        string
}

// NewRegistry returns a new default client
func NewRegistry() *Registry {
	return &Registry{
		HTTPClient: http.DefaultClient,
		URL:        "https://registry.npmjs.org/",
	}
}

// GetPackage from registry
func (r *Registry) GetPackage(name string) (*Package, error) {
	resp, err := r.HTTPClient.Get(r.URL + name)
	if err != nil {
		return nil, fmt.Errorf("failed to get packet information: %v", err)
	}
	defer resp.Body.Close()

	// parse response
	var pack *Package
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&pack)
	if err != nil {
		return nil, fmt.Errorf("invalid response: %v", err)
	}

	// add registry reference
	for _, p := range pack.Versions {
		p.registry = r
	}
	return pack, nil
}

// GetPackageVersion from registry
func (r *Registry) GetPackageVersion(name, version string) (*PackageVersion, error) {
	resp, err := r.HTTPClient.Get(r.URL + name + "/" + version)
	if err != nil {
		return nil, fmt.Errorf("failed to get packet information: %v", err)
	}
	defer resp.Body.Close()

	// parse response
	var pack *PackageVersion
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&pack)
	if err != nil {
		return nil, fmt.Errorf("invalid response: %v", err)
	}

	// add registry reference
	pack.registry = r
	return pack, nil
}

// Package information
type Package struct {
	Name        string                     `json:"name"`
	Description string                     `json:"description"`
	Versions    map[string]*PackageVersion `json:"versions"`
}

// GetLatestVersion of package based on constraint
func (p *Package) GetLatestVersion(constraint string) (*PackageVersion, error) {
	// validate and parse constraint
	constr, err := semver.NewConstraint(constraint)
	if err != nil {
		return nil, fmt.Errorf("invalid constraint for %s: %v", p.Name, err)
	}

	// get matching versions
	entries := make([]*PackageVersion, 0)
	for _, entry := range p.Versions {
		if constr.Check(entry.Version) {
			entries = append(entries, entry)
		}
	}
	if len(entries) == 0 {
		return nil, fmt.Errorf("no version found for %s", p.Name)
	}

	// get latest matching version
	sort.Slice(entries, func(i, j int) bool {
		return entries[i].Version.GreaterThan(entries[j].Version)
	})

	return entries[0], nil
}

// PackageVersion information
type PackageVersion struct {
	Name        string          `json:"name"`
	Description string          `json:"description"`
	Version     *semver.Version `json:"version"`

	Dist struct {
		Shasum  string `json:"shasum"`
		Tarball string `json:"tarball"`
	} `json:"dist"`

	registry *Registry
}

// Exist checks if package version exists in the directory
func (p *PackageVersion) Exist(dstDir string) bool {
	data, err := ioutil.ReadFile(dstDir + ".hash")
	return err == nil && string(data) == p.Dist.Shasum
}

// Download package version to directory
func (p *PackageVersion) Download(dstDir string) error {
	// remove old data if exist
	if _, err := os.Stat(dstDir); err == nil {
		zap.S().Debugf("remove old %s", p.Name)
		err = os.RemoveAll(dstDir)
		if err != nil {
			return fmt.Errorf("failed to remove old data: %v", err)
		}
	}

	err := os.MkdirAll(dstDir, os.ModePerm)
	if err != nil {
		return fmt.Errorf("failed to create directory %s: %v", dstDir, err)
	}

	// download artifact
	zap.S().Debugf("download & extract %s", p.Dist.Tarball)
	resp, err := p.registry.HTTPClient.Get(p.Dist.Tarball)
	if err != nil {
		return fmt.Errorf("failed to get %s: %v", p.Dist.Tarball, err)
	}
	defer resp.Body.Close()

	// extract archive
	err = extract.Archive(context.Background(), resp.Body, dstDir, func(name string) string {
		// remove package sub directory from path
		if strings.HasPrefix(name, "package/") {
			return name[8:]
		}
		return name
	})
	if err != nil {
		return fmt.Errorf("failed to extract %s", p.Dist.Tarball)
	}

	// update has file
	return ioutil.WriteFile(dstDir+".hash", []byte(p.Dist.Shasum), os.ModePerm)
}
