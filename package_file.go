// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package npm

import (
	"encoding/json"
	"fmt"
	"os"
)

// PackageFile package information loaded from file
type PackageFile struct {
	Name         string            `json:"name"`
	Version      string            `json:"version"`
	Dependencies map[string]string `json:"dependencies"`
}

// LoadPackageFile from package.json
func LoadPackageFile(path string) (*PackageFile, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("failed to open package file: %v", err)
	}
	defer file.Close()

	var pack *PackageFile
	err = json.NewDecoder(file).Decode(&pack)
	if err != nil {
		return nil, fmt.Errorf("failed to parse package file: %v", err)
	}

	return pack, nil
}

// PackageLockFile contains content of package-lock.json
type PackageLockFile map[string]interface{}

// NewPackageLockFile data from name and version
func NewPackageLockFile(name, version string) *PackageLockFile {
	return &PackageLockFile{
		"name":         name,
		"version":      version,
		"dependencies": make(map[string]interface{}),
	}
}

// GetDependencyVersion of dependency with the given name
func (p *PackageLockFile) GetDependencyVersion(name string) (string, bool) {
	dependencies, ok := (*p)["dependencies"].(map[string]interface{})
	if !ok {
		return "", false
	}

	dependency, ok := dependencies[name].(map[string]interface{})
	if !ok {
		return "", false
	}

	version, ok := dependency["version"].(string)
	return version, ok
}

// SetDependencyVersion for dependency with the given name
func (p *PackageLockFile) SetDependencyVersion(name, version string) {
	dependencies, ok := (*p)["dependencies"].(map[string]interface{})
	if !ok {
		dependencies = make(map[string]interface{})
		(*p)["dependencies"] = dependencies
	}

	dependency, ok := dependencies[name].(map[string]interface{})
	if !ok {
		dependency = make(map[string]interface{})
		dependencies[name] = dependency
	}

	dependency["version"] = version
}

// LoadPackageLockFile content from file
func LoadPackageLockFile(path string) (*PackageLockFile, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("failed to open package lock file: %v", err)
	}
	defer file.Close()

	var pack *PackageLockFile
	err = json.NewDecoder(file).Decode(&pack)
	if err != nil {
		return nil, fmt.Errorf("failed to parse package lock file: %v", err)
	}
	return pack, nil
}

// SavePackageLockFile content to file
func SavePackageLockFile(path string, pack *PackageLockFile) error {
	file, err := os.Create(path)
	if err != nil {
		return fmt.Errorf("failed to open package lock file: %v", err)
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	encoder.SetIndent("", "  ")
	err = encoder.Encode(&pack)
	if err != nil {
		return fmt.Errorf("failed to parse package lock file: %v", err)
	}
	return nil
}
