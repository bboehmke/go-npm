# go-npm

Simple NPM registry client written in go.


## NPM Loader
> Note: This tool works similar to `npm install` but with some differences!

The NPM Loader can be used to download packages from NPM registry without a 
Node.js installation.

Simply call the app in the directory containing the `package.json`:
```bash
go run gitlab.com/bboehmke/go-npm/cmd/npm_loader
```
This will download the defined packages to `dist/`.

This tool can also be called via `go:generate`:
```go
//go:generate go run gitlab.com/bboehmke/go-npm/cmd/npm_loader --base_dir ../ --dst_dir ./web/assets/
```

In this case the working directory is changed to `../` and the packages where 
downloaded to `./web/assets/` (relative to working directory).

## CLI help

```
Usage of npm_loader:
  -base_dir string
        base directory with project.json (default "/home/user/GoProjects/go-npm")
  -dst_dir string
        destination directory for packages (default "dist/")
  -update_lock
        update lock file
  -verbose
        verbose console logging
```