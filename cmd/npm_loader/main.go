// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"flag"
	"os"
	"path/filepath"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/bboehmke/go-npm"
)

func main() {
	currentDir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	// command line arguments
	updateLock := flag.Bool("update_lock", false, "update lock file")
	verbose := flag.Bool("verbose", false, "verbose console logging")
	baseDir := flag.String("base_dir", currentDir, "base directory with project.json")
	dstDir := flag.String("dst_dir", "dist/", "destination directory for packages")
	flag.Parse()

	level := zapcore.InfoLevel
	if *verbose {
		level = zapcore.DebugLevel
	}

	// prepare logger
	logger := zap.New(
		zapcore.NewCore(
			zapcore.NewConsoleEncoder(zap.NewDevelopmentEncoderConfig()),
			zapcore.Lock(os.Stdout),
			level))
	zap.ReplaceGlobals(logger)
	defer logger.Sync()

	// start download
	registry := npm.NewRegistry()
	err = registry.DownloadDependencies(
		*baseDir, filepath.Join(*baseDir, *dstDir), *updateLock)
	if err != nil {
		zap.L().Fatal(err.Error())
	}
}
